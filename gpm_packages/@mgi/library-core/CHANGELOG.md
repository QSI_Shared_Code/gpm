## 0.4.0

- Added `Tolopolical Sort`
- Removed duplicate VI `Array of Enum Values` (Equivalent to Enumerate Enum)


## 0.3.0

- Added `GUIDv4`
- Added `Enumerate Enum`

## 0.2.0

- Reworked enable if/gray if to be less confusing
- Added `Array of Enum Values.vim`
- Reformatted readme.

## 0.1.2

- Added several array helper VIs

## 0.1.1

- Updated Readme and Relinked VIs

## 0.1.0

- Initial Release.
